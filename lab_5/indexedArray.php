<?php
$month = array ('January', 'February', 'March', 'April',
'May', 'June', 'July', 'August','September', 'October', 'November', 'December');

// question 1 - for loop
$mLength = count ($month);
  for($x=0; $x<$mLength; $x++)
  echo '$month' . "[$x]:" . $month[$x];  
  echo "</br>". "</br>";

// question 2 - sorting array 
arsort($month);
$arrlength = count($month);

for($x = 0; $x < $arrlength; $x++) {
  echo $month[$x] . "</br>";
}

// question 3 - foreach loop
foreach ($month as $index => $value) {
  echo "No =" . $index . " Month = " . $value . "<br>";
}
?>
