<?php

$monthDays = array ( 'January' => '31', 'February' => '28',
                    'March' => '31', 'April' => '30',
                    'May' => '31', 'June' => '30',
                    'July' => '31', 'August' => '31', 
                    'September' => '30', 'October' => '31',
                    'November' => '30', 'December' => '31');

// question 4(b)
foreach($monthDays as $month => $days) {
echo "MONTH = " . $month . " , DAY = " . $days;
echo "<br>";
}      

// question 4(c)
echo "</br> <b>This is months that have 30 days: </b> </br>";
foreach($monthDays as $month => $days) {
   if($days==30){
   echo "Month= " . $month;
   echo "<br>"; };
}

?>


